#!/usr/bin/env python

import RPi.GPIO as gpio
import time
from random import randrange
import picture_arcade as picture_arcade



output_pins = [ 13,26,20,21 ] 
input_pins = [  27,6,22,5 ] 
led_on_array = []

outpin = 21
interval = 5

gpio.setmode(gpio.BCM)
#gpio.setmode(gpio.BOARD)

def led_on(button):
    gpio.output(button, True)
#    import pdb
#    pdb.set_trace()
    led_on_array.append(button)


def led_off(button):
    gpio.output(button, False)
    led_on_array.remove(button)

def is_button_press():
    pressed_buttons = []
    for button in input_pins:
        if gpio.input(button) == gpio.HIGH:
            pressed_buttons.append(button)
    return pressed_buttons


def rotate_lights(speed):
    for led in output_pins:
        led_on(led)
        time.sleep(0.05 * int(speed))
        led_off(led)


def is_pressed_with_light(button_number,timeout_ms):
    press_on_time = False
    pressed_buttons = []
    for x in range(timeout_ms):
        time.sleep(0.001)
        pressed_buttons = is_button_press()
        if pressed_buttons:
            for y in pressed_buttons:
               position = input_pins.index(y)
               if output_pins[position] == button_number:
#                       print str(pressed_buttons)[1:-1]  
                       if output_pins[position] in led_on_array:
                           press_on_time = True
                           break;
        if press_on_time:
            break;
    # Finish Loop
    return press_on_time

def rotate_lights_check_if_pressed(speed):
    for led in output_pins:
        led_on(led)
        if is_pressed_with_light(led,speed * 1000):
            print "rotate_lights_check_if_preseed:" + str(led) +  " Press when was light"
            picture_arcade.pick_random_picture_show_it(myPicDict,'./downloads/')

        else:
            print "rotate_lights_check_if_pressed: " + str(led) +  " Did not press on time"
        led_off(led)


def random_rotate_lights_check_if_pressed(speed):
    number_of_elements = len(output_pins)
    led_number = output_pins[randrange(number_of_elements)]
    led_on(led_number)
    if is_pressed_with_light(led_number,speed * 1000):
        print "random_rotate_lights_check_if_pressed:" + str(led_number) +  " Press when was light"
        picture_arcade.pick_random_picture_show_it(myPicDict,'./downloads/')
    else:
        print "random_rotate_lights_check_if_preseed:" + str(led_number) +  " Did not press on time"
    led_off(led_number)




# Init Input pins    
for button in input_pins:
    gpio.setup(button,gpio.IN,  pull_up_down=gpio.PUD_DOWN)

# Init Output Pins
for led in output_pins:
    gpio.setup(led, gpio.OUT)
    gpio.output(led, False)


#Load Pictures
myPicDict = {}
myPicDict = picture_arcade.scan_folder_with_pictures('./downloads/')

rotate_lights_check_if_pressed(5)

while True:
    random_rotate_lights_check_if_pressed(10)
